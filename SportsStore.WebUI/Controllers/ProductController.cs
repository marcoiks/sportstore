﻿using SportsStore.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.WebUI.Models;

namespace SportsStore.WebUI.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repo;
        public int pagesize = 4;
        // GET: Product
        public ProductController(IProductRepository repo)
        {
            this.repo = repo;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult List(string category, int page = 1)
        {
            ProductsListViewModel model = new ProductsListViewModel
            {
                Products = repo.Products.Where(p => p.Category == null || p.Category ==  category)
                .OrderBy(p => p.ProductID)
                .Skip((page - 1) * pagesize)
                .Take(pagesize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pagesize,
                    TotalItems = repo.Products.Count()
                },
                CurrentCategory = category
            };
            
            return View(model);

        }
    }
}